//
//  Person+Enums.swift
//  FABCasting
//
//  Created by Gandhale, Gaurav on 9/21/17.
//  Copyright © 2017 Gandhale, Gaurav. All rights reserved.
//

import Foundation

enum Gender {
    case male
    case female
    case other
}

enum HairColor {
    case black
    case other
}

enum EyeColor {
    case black
    case other
}

enum Ethinicity {
    case white
    case black
    case asian
    case hispanic
    case mixed
    case other
}

enum Importance {
    case registered
    case complete
    case publised
    case promoted
    case mature
    case vip
}

enum WorkStatus {
    case available
    case signed
    case vacation
}
