//
//  Person.swift
//  FABCasting
//
//  Created by Gandhale, Gaurav on 9/20/17.
//  Copyright © 2017 Gandhale, Gaurav. All rights reserved.
//

import Foundation
import ObjectMapper

class Person: Mappable {

    var userToken: String?
    var email: String?
    
    var firstName: String?
    var lastName: String?
    var displayName: String?
    
    var dateOfBirth: String?
    var gender: Gender?
    
    var hairColor: HairColor?
    var eyeColor: EyeColor?
    
    var height: Double? //In centimeters
    var weight: Double? //In Kg
    
    var chest: Double? //In centimeters
    var hips: Double? //In centimeters
    var waist: Double? //In centimeters
    
    var city: String?
    var state: String?
    var country: String?
    var latitude: String?
    var longitude: String?
    
    var websites: [String]?
    var aboutMe: String?
    var contactNumber: String?
    
    var hourRate: Int?
    var halfDayRate: Int?
    var fullDayRate: Int?
    var overNightRate: Int?
    var commission: Int?
    
    var willTravel: Bool?
    var travelDistance: Int? //Km
    
    var booked: [NSDate]?
    var available: [NSDate]?
    var availableTravelDates: [NSDate]?
    
    var importance: Importance?
    var status: WorkStatus?
    var workingWith: [Person]?
    
    var photos: [String]?
    var profilePhoto: String?
    var rating: Int?
    var ratingBy: Int?
    
    required init?(map: Map) {
    }

    func mapping(map: Map) {
    }
}
